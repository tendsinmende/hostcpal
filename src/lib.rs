//!Small cpal host that abstracts host and device creation, as well as enforces a certain sampling format and type of audio stream handling.
//!
//! Note that the crate is highly opinionated.

use cpal::{HostId, SampleRate, Stream, StreamConfig, SupportedBufferSize, SupportedStreamConfig, traits::{DeviceTrait, HostTrait, StreamTrait}};

pub extern crate cpal;


pub struct Host{
    ///The host used for creating the device. For instance on windows this would be either WASAPI or ASIO, on Linux this is ALSA or Jack.
    host: cpal::Host,
}

impl Host{
    ///Creates the host on a given ID.
    pub fn new(id: HostId) -> Result<Self, HostError>{
	let new_host = if let Ok(h) = cpal::host_from_id(id){
	    h
	}else{
	    return Err(HostError::NoSuchHostId)
	};
	Ok(Host{
	    host: new_host
	})
    }

    pub fn new_default() -> Self{
	Host{
	    host: cpal::default_host()
	}
    }

    ///tries to find a output device that fulfills the given
    ///arguments. If no host was created yet, creates the default host.
    pub fn new_device_with_properties(&self, sampling_rate: usize, buffer_size: usize, channels: u16) -> Result<Device, HostError>{
	//Now try to find a satisfying device.
	if let Ok(devices) = self.host.output_devices(){
	    for d in devices{
		if let Ok(configs) = d.supported_output_configs(){
		    for c in configs{
			if c.min_sample_rate().0 as usize <= sampling_rate && c.max_sample_rate().0 as usize > sampling_rate
			    && in_buffer_range(c.buffer_size(), buffer_size) && c.channels() >= channels{
				let config = c.with_sample_rate(SampleRate(sampling_rate as u32));
				return Ok(Device{
				    output_config: config,
				    device: d,
				    stream: None,
				});
			    }
		    }
		}//else continue, device seems to be unplugged since we created the host.
	    }
	}else{
	    return Err(HostError::NoOutputDevices);
	}
	
	Err(HostError::NoSuchOutputDevice)
    }
    
    ///Returns a list of device names that are available.
    pub fn devices(&self) -> Vec<String>{
	if let Ok(ds) = self.host.output_devices(){
	    ds.map(|dev| dev.name().unwrap_or(String::from("Unknown"))).collect()
	}else{
	    vec![]
	}
    }
    
    pub fn new_default_device(&mut self) -> Result<Device, HostError>{
	if let Some(dd) = self.host.default_output_device(){
	    let output_config = if let Ok(c) = dd.default_output_config(){
		c
	    }else{
		return Err(HostError::NoOutputDevices);
	    };
	    Ok(Device{
		device: dd,
		output_config,
		stream: None,
	    })
	}else{
	    Err(HostError::NoOutputDevices)
	}
    }
}

#[derive(Debug, Clone, Copy)]
pub enum HostError{
    NoSuchHostId,
    NoSuchOutputDevice,
    NoOutputDevices,
    NoSuchInputDevice,
    NoInputDevices,
}

pub struct Frame{
    pub channels: Vec<Vec<f32>>,
    pub sample_rate: usize,
}

impl Frame{
    pub fn channels(&self) -> usize{
	self.channels.len()
    }

    pub fn buffer_size(&self) -> usize{
	//We know that all channels have the same buffer size
	self.channels[0].len()
    }
}

#[derive(Debug, Clone, Copy)]
pub enum DeviceError{
    FailedToPlay,
    FailedToPause,
    FailedToSetNewPlayer,
    FailedToStopOldStream,
}

///Device and config on some host. Note that the config can be changed within a certain range.
pub struct Device{
    device: cpal::Device,
    output_config: SupportedStreamConfig,
    //Inner stream handle that is used to control the playing thread.
    stream: Option<Stream>,
}

impl Device{
    pub fn set_player(&mut self, player: impl Playable + Send + 'static) -> Result<(), DeviceError>{
	let config = self.output_config.clone().into();
	let new_stream = match self.output_config.sample_format(){
	    cpal::SampleFormat::F32 => stream_builder::<f32>(&self.device, &config, Box::new(player))?,
	    cpal::SampleFormat::I16 => stream_builder::<i16>(&self.device, &config, Box::new(player))?,
	    cpal::SampleFormat::U16 => stream_builder::<u16>(&self.device, &config, Box::new(player))?,
	};
	
	self.stream = Some(new_stream);
	Ok(())
    }

    pub fn pause(&mut self) -> Result<(), DeviceError>{
	if let Some(s) = &mut self.stream{
	    if s.pause().is_err(){
		Err(DeviceError::FailedToPause)
	    }else{
		Ok(())
	    }
	}else{
	    Ok(())
	}
    }

    ///Plays the stream. Returns true on success or false if either not player is set, or playing failed.
    pub fn play(&mut self) -> Result<(), DeviceError>{
	if let Some(s) = &mut self.stream{
	    if s.play().is_err(){
		Err(DeviceError::FailedToPlay)
	    }else{
		Ok(())
	    }
	}else{
	    Ok(())
	}
    }
}

fn stream_builder<T: cpal::Sample>(device: &cpal::Device, config: &StreamConfig, mut player: Box<dyn Playable + Send>) -> Result<Stream, DeviceError>{

    let mut edit_buffer = Vec::with_capacity(256);
    let num_channel = config.channels as usize;
    let sample_rate = config.sample_rate.0 as usize;
    
    if let Ok(stream) = device.build_output_stream(
	config,
	move |data: &mut [T], _: &cpal::OutputCallbackInfo|{
	    if edit_buffer.len() < data.len(){
		//Grow edit buffer if needed
		edit_buffer.resize(data.len(), 0.0);
	    }
	    player.process(&mut edit_buffer[0..data.len()], num_channel, sample_rate);
	    sample_copy(data, &edit_buffer[0..data.len()]);
	},
	|err| eprintln!("Cpal stream error occured: {}", err)
    ){
	Ok(stream)
    }else{
	return Err(DeviceError::FailedToSetNewPlayer);
    }
}

///Converter function to copy processblock from one type to another
#[inline]
fn sample_copy<T: cpal::Sample>(target_buffer: &mut [T], src_buffer: &[f32]){
    for (target, src) in target_buffer.iter_mut().zip(src_buffer.iter()){
	*target = cpal::Sample::from::<f32>(&src);
    }
}

pub trait Playable{
    ///called when a new buffer should be processed.
    fn process(&mut self, data: &mut [f32], channels: usize, sample_rate: usize);
    ///Should return true, if the output calculated in `process` is limited to a range from
    /// -1 to 1. Otherwise the output will be clamped by the host before writing it to the outputting device.
    fn is_clamped(&self) -> bool{
	false
    }
}

fn in_buffer_range(range: &SupportedBufferSize, size: usize) -> bool{
    match range{
	SupportedBufferSize::Unknown => false,
	SupportedBufferSize::Range{min, max} => (*min as usize) < size && (*max as usize) > size,
    }
}
